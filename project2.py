from gpiozero import Button, LED
from time import sleep

red = LED(17)
button = Button(9)

def welcome():
    print("Hello!, can you see the LED flashing?")
    red.blink(0.2,0.2)
    sleep(5)
    red.off()

while True:
    button.when_pressed = welcome

from gpiozero import TrafficLights, Button
from time import sleep

red = 17
amber = 27
green = 22
crossing = Button(9)

lights = TrafficLights(red, amber, green)

lights.green.on()

while True:
    if crossing.is_pressed:
        lights.amber.blink()
        lights.green.off()
        lights.red.on()
        sleep(5)
    else:
        sleep(5)
        lights.green.off()
        lights.amber.on()
        sleep(1)
        lights.amber.off()
        lights.red.on()
        sleep(5)
        lights.amber.on()
        sleep(1)
        lights.green.on()
        lights.amber.off()
        lights.red.off()
